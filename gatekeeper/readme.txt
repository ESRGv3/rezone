This is the gatekeeper for ReZone.

It was built using SDK version SDK_2.8.0_EVK-MIMX8MQ and compiler gcc-arm-none-eabi version 9.3.1.

See the following link for instructions on how to setup the SDK https://www.nxp.com/document/guide/getting-started-with-the-mcimx8m-evk:GS-MCIMX8M-EVK.

Put this project into <SDK_dir>/boards/evkmimx8mq/gatekeeper

Compile by running `./build_release.sh` in armgcc directory
