/*
 * Copyright 2017-2019 NXP
 * All rights reserved.
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#include "board.h"
#include "fsl_debug_console.h"
#include "fsl_rdc.h"
#include "fsl_rdc_sema42.h"
#include "fsl_mu.h"

#include "pin_mux.h"
#include "clock_config.h"
#include "fsl_gpio.h"
/*******************************************************************************
 * Definitions
 ******************************************************************************/

/*
 * Master index:
 * All masters excluding ARM core: 0
 * A53 core: 1
 * M4 core: 6
 * SDMA 3
 */
#define APP_MASTER_INDEX 6

enum RZ_CTRL {
    RZ_CTR_TOKEN	= 0,
    RZ_CTRL_SWITCH	= 1,
};

/*******************************************************************************
 * Variables
 ******************************************************************************/
rdc_domain_assignment_t assignment;
rdc_periph_access_config_t periphConfig;
rdc_mem_access_config_t memConfig;


typedef union {
    struct{
	uint32_t h;
	uint32_t l;
    };
    uint64_t val;
} u64;

/*******************************************************************************
 * Code
 ******************************************************************************/
static void Fault_Handler(void)
{
    __DSB();
}

void HardFault_Handler(void)
{
    Fault_Handler();
}

void BusFault_Handler(void)
{
    Fault_Handler();
}

static int initialized = 0;
u64 token = { .h = 0xd00ddead, .l = 0xbabecafe };

void rz_handle_get_token(void)
{
    if(initialized)
	return;

    /* TODO: create token using TRNG hardware */
    MU_SendMsg(MUB, 0, token.l);
    MU_SendMsg(MUB, 0, token.h);
    initialized = 1;
}


void rz_handle_switch()
{
    uint32_t on = MU_ReceiveMsg(MUB, 0x0);
    RDC_GetDefaultPeriphAccessConfig(&periphConfig);
    periphConfig.periph = kRDC_Periph_RDC;

    if(on){
	/* Make peripheral not accessible. */
	periphConfig.policy &= ~(RDC_ACCESS_POLICY(AP_DOMAIN_ID, kRDC_ReadWrite));
	periphConfig.policy |= (RDC_ACCESS_POLICY(OTHER_DOMAIN_ID, kRDC_ReadWrite));
	RDC_SetPeriphAccessConfig(RDC, &periphConfig);
    } else {
	periphConfig.policy |= (RDC_ACCESS_POLICY(AP_DOMAIN_ID, kRDC_ReadWrite));
	periphConfig.policy |= (RDC_ACCESS_POLICY(OTHER_DOMAIN_ID, kRDC_ReadWrite));
	RDC_SetPeriphAccessConfig(RDC, &periphConfig);
    }

    MU_SendMsg(MUB, 0, 0x0);
}

u64 token_get(void)
{
    u64 t;
    t.l = MU_ReceiveMsg(MUB, 0x0);
    t.h = MU_ReceiveMsg(MUB, 0x0);

    return t;
}

/*!
 * @brief Main function
 */
int main(void)
{
    /* Board specific RDC settings */
    BOARD_RdcInit();

    BOARD_InitPins();
    BOARD_BootClockRUN();
    BOARD_InitDebugConsole();
    BOARD_InitMemory();

    /* Set the IOMUXC_GPR10[2:3], thus the memory violation triggers the hardfault. */
    *(volatile uint32_t *)0x30340028 |= (0x0C);

    MU_Init(MUB);

    PRINTF("\r\nReZone controller\r\n");

    RDC_Init(RDC);
    PRINTF("RDC initialized\n");


    RDC_GetDefaultPeriphAccessConfig(&periphConfig);
    periphConfig.periph = kRDC_Periph_RDC;

    /* Make peripheral not accessible. */
    periphConfig.policy |= (RDC_ACCESS_POLICY(AP_DOMAIN_ID, kRDC_ReadWrite));
    RDC_SetPeriphAccessConfig(RDC, &periphConfig);


    /* Assign current master domain. */
    /* assign a53 to domain */
    RDC_GetDefaultMasterDomainAssignment(&assignment);
    assignment.domainId = AP_DOMAIN_ID;
    RDC_SetMasterDomainAssignment(RDC, kRDC_Master_A53, &assignment);
    PRINTF("AP domain established\n");

    RDC_GetDefaultMasterDomainAssignment(&assignment);
    assignment.domainId = BOARD_DOMAIN_ID;
    RDC_SetMasterDomainAssignment(RDC, kRDC_Master_M4, &assignment);
    PRINTF("gatekeeper domain established\n");

    for (int i = kRDC_Master_PCIE1; i <= kRDC_Master_SDMA1_SPBA1; ++i) {
	RDC_GetDefaultMasterDomainAssignment(&assignment);
	assignment.domainId = OTHER_DOMAIN_ID;
	RDC_SetMasterDomainAssignment(RDC, i, &assignment);
    }

    PRINTF("Domains established\n");

    PRINTF("Entering loop\n");

    while (1)
    {
	uint32_t received = -1;
	u64 token_check;

	if(!initialized){
	    received = MU_ReceiveMsg(MUB, 0x0);
	    if(received == 0x0){ /* get token id */
		/* this is never callable again */
		rz_handle_get_token();
	    }
	    continue;
	}

	token_check = token_get();
	if(token_check.val != token.val){
	    PRINTF("Incorrect token. Got 0x%x%x, expected 0x%x%x, attacker likely\n", token_check.h, token_check.l, token.h, token.l);
	    continue;
	}

	received = MU_ReceiveMsg(MUB, 0x0);

	switch(received){
	    case RZ_CTRL_SWITCH:
		rz_handle_switch();
		break;
	    default:
		PRINTF("\nUnknown %d\n", received);
	}
    }
}

static inline uint32_t adjust_ddr(uint32_t addr)
{
    addr = addr - 0x40000000;
    return addr;
}

