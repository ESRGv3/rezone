#include <utils_def.h>
#include <stdint_.h>

typedef enum
{
	RZ_NoAccess  = 0b00000000, /*!< AP Could not read or write. */
	RZ_WriteOnly = 0b00000001, /*!< AP Write only. */
	RZ_ReadOnly  = 0b00000010, /*!< AP Read only. */
	RZ_ReadWrite = 0b00000011, /*!< AP Read and write. */
} rz_policy;


#ifdef REZONE
IMPORT_SYM(unsigned long, __rz_trampoline_start, RZ_TRAMPOLINE_START);
IMPORT_SYM(unsigned long, __rz_trampoline_end, RZ_TRAMPOLINE_END);
IMPORT_SYM(unsigned long, __rz_scratchpad_start, RZ_SCRATCHPAD_START);
IMPORT_SYM(unsigned long, __rz_scratchpad_end, RZ_SCRATCHPAD_END);
IMPORT_SYM(unsigned long, __rz_coher_start, RZ_COHER_START);
IMPORT_SYM(unsigned long, __rz_coher_end, RZ_COHER_END);
IMPORT_SYM(unsigned long, __rz_uncached_ex_start,	RZ_UNCACHED_EX_START);
IMPORT_SYM(unsigned long, __rz_uncached_ex_end,	RZ_UNCACHED_EX_END);
#endif

extern uint64_t rz_token;
extern uint64_t rz_tee_id;
extern uint64_t rz_last_tee_id;

#ifdef REZONE
# define __REZONE __attribute__((section(".rezone_trampoline")))
#else
# define __REZONE
#endif

void rz_psci_mon_in_use(int use, int cpuid);
void rz_psci_w8ing_room(void);
void rz_psci_power_down_wfi();

void rz_mon_in_use_inc(void);
void rz_mon_in_use_dec(void);

void rz_wait_wfe();
void rz_wait_wfi();
void rz_wait();
void rezone_normal_world_suspend(void);
void rezone_init(void);

void rz_get_secret_token(uint64_t *token);

void rz_register_range(uint32_t zone_id, uint32_t id, uint64_t start_addr,
		uint32_t size, uint32_t policy);
void rz_update_range_policy(uint32_t zone_id, uint32_t id, uint32_t policy);
void rz_register_periph(uint32_t zone_id, uint32_t id, uint32_t policy);
void rz_pre_activate_zone(uint32_t id);
void rz_periph_enforce();
void rz_make_sure();
void rz_asser_lock();


void rz_mon_in_use_inc_from_sw(void);
void rz_mon_prot(void);
void rz_mon_unprot(void);
void rz_lock_rz_and_mon_in_use_wait_zero(void);
