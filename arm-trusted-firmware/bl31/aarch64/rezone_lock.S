#include <asm_macros.S>
#include "rezone_macros.S"

.global ticket
.global entering
.global rz_bakery_lock
.global rz_bakery_unlock
.global rz_lock_priv
.section .rezone_trampoline, "ax"

.section .rz_coher
rz_lock_priv:	.long 0

.section .rezone_trampoline, "ax"

rz_func rz_bakery_lock
	ldr	x0, =rz_lock_priv
	mov	w2, #1
	sevl
1:	wfe
2:	ldaxr	w1, [x0]
	cbnz	w1, 1b
	stxr	w1, w2, [x0]
	cbnz	w1, 2b
	dsb sy
	ret
rz_endfunc rz_bakery_lock

rz_func rz_bakery_unlock
	ldr	x0, =rz_lock_priv
	stlr	wzr, [x0]
	dsb sy
	sev
	ret
rz_endfunc rz_bakery_unlock

