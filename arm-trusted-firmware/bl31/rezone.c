#include <spinlock.h>
#include <interrupt_mgmt.h>
#include <ep_info.h>
#include <debug.h>
#include <platform_def.h>
#include <platform.h>
#include <arch_helpers.h>
#include <rezone.h>
#include <imx8_mu.h>

spinlock_t rz_suspend_lock;
static volatile uint32_t wfe_done;
volatile uint64_t is_call_done;

#ifndef REZONE_NO_NW_PROTECTION
static __REZONE uint64_t
rezone_nw_suspend_wait(uint32_t id, uint32_t flags,
		void *handle, void *cookie)
{
	(void)id;
	(void)flags;
	(void)handle;
	(void)cookie;

	uint32_t irq;
	irq = plat_ic_acknowledge_interrupt();
	if (irq < 1022U) {
		plat_ic_end_of_interrupt(irq);
	}

	return 0;
}

extern uint32_t rz_mon_in_use_counter;
extern void rz_lock();
extern void rz_unlock();

static void __REZONE lock_and_wait_zero()
{
	while(1){
		rz_mon_in_use_dec();
		rz_lock();
		/* wait until not in use */
		if(!rz_mon_in_use_counter)
			break;

		rz_unlock();
		wfe();
	}
	/* from this point on we are the only core executing in monitor */
	isb();
	rz_asser_lock();
}

extern volatile uint32_t rz_w8ing_for_mon_counter;

void rezone_normal_world_suspend()
{
	uint64_t cpu_id = plat_my_core_pos();
	is_call_done = 0;

	lock_and_wait_zero();

	/* trigger the SGI to notify other cores */
	for (int i = 0; i < PLATFORM_CORE_COUNT; i++)
		if(i != cpu_id){
			plat_ic_raise_el3_sgi(0x9, i);
			rz_asser_lock();
		}

	/* make sure everyone is waiting */
	while(1) {
		rz_asser_lock();
		uint32_t we_count_as_w8ing =
			(rz_w8ing_for_mon_counter | 1 << cpu_id) &
			(~rz_mon_in_use_counter & 0b1111);

		bool are_all_w8ing = ((we_count_as_w8ing) & 0b1111) == 0b1111;

		/* everybody is waiting for this core to execute trustedOS */
		if(are_all_w8ing)
			break;
		/* trigger the SGI to notify other cores */
		for (int i = 0; i < PLATFORM_CORE_COUNT; i++){
			if(!((we_count_as_w8ing & 0b1111) & (1 << i)))
				plat_ic_raise_el3_sgi(0x9, i);
		}
	}

	/* dcsw_op_all(DCCSW); // do this before jumping to trustedOS */
}

void rezone_normal_world_resume()
{
	/* resume */
	wfe_done = 0;
	is_call_done = 1;
	dsb();
	sev();
	isb();
}
#endif

extern uint64_t timetoflush;
void rezone_init()
{
	timetoflush = 0;

	/* obtain secret token */
	rz_get_secret_token(&rz_token);

	// specify_zone1();
	// specify_zone2();

#ifndef REZONE_NO_NW_PROTECTION
	uint64_t flags = 0;
	uint64_t rc;
	set_interrupt_rm_flag(flags, INTR_TYPE_NON_SECURE);
	rc = register_interrupt_type_handler(INTR_TYPE_EL3,
			rezone_nw_suspend_wait, flags);

	if(rc)
		panic();
#endif
}

enum RZ_CTRL {
	RZ_CTR_TOKEN          = 0,
	RZ_CTRL_SWITCH        = 1,
};


void rz_get_secret_token(uint64_t *token)
{
	MU_SendMessage(0x30AA0000u, 0x0, RZ_CTR_TOKEN);
	uint32_t token_l = -1;
	uint32_t token_h = -1;
	MU_ReceiveMsg(0x30AA0000u, 0x0, &token_l);
	MU_ReceiveMsg(0x30AA0000u, 0x0, &token_h);
	*token = ((uint64_t)token_h << 32) | token_l;
	NOTICE("Secret token is: 0x%llx\n", *token);
}

void rz_pre_activate_zone(uint32_t id)
{
	rz_tee_id = id;
}

