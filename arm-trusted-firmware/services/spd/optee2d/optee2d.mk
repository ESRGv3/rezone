#
# Copyright (c) 2013-2014, ARM Limited and Contributors. All rights reserved.
#
# SPDX-License-Identifier: BSD-3-Clause
#

OPTEE2D_DIR		:=	services/spd/optee2d
SPD2_INCLUDES		:=

SPD2_SOURCES		:=	services/spd/optee2d/optee2d_common.c	\
				services/spd/optee2d/optee2d_helpers.S	\
				services/spd/optee2d/optee2d_main.c	\
				services/spd/optee2d/optee2d_pm.c

NEED_BL32		:=	yes
